<?php

use Symfony\Component\Routing;

$routes = new Routing\RouteCollection();
$routes->add('hello', new Routing\Route("/{name}", array("name"=>"Gustavo") ));
$routes->add('Bye', new Routing\Route("/bye/{name}", array("name"=>"Gustavo")) );

return $routes;